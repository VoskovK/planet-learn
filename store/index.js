export const state = () => ({
  isHideModal: false
});

export const mutations = { 
  showModal(state, value) {
    state.isHideModal = value
  }
};